#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.predicate;

import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.connect.connector.ConnectRecord;
import org.apache.kafka.connect.transforms.predicates.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class ${classnamePrefix}Predicate<R extends ConnectRecord<R>> implements Predicate<R> {
    private static final Logger LOGGER = LoggerFactory.getLogger(${classnamePrefix}Predicate.class);
    private ${classnamePrefix}PredicateConfig config;

    @Override
    public ConfigDef config() {
        return ${classnamePrefix}PredicateConfig.configDef();
    }

    @Override
    public boolean test(R record) {
        LOGGER.debug("Evaluating predicate");
        return false;
    }

    @Override
    public void close() {
        LOGGER.debug("Closing predicate");
        // Close any resource you might have open
    }

    @Override
    public void configure(Map<String, ?> configs) {
        LOGGER.debug("Configuring predicate");
        config = new ${classnamePrefix}PredicateConfig(configs);
    }
}
