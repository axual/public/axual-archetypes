#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.transformation;

import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.connect.connector.ConnectRecord;
import org.apache.kafka.connect.transforms.Transformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class ${classnamePrefix}Transformation<R extends ConnectRecord<R>> implements Transformation<R> {
    private static final Logger LOGGER = LoggerFactory.getLogger(${classnamePrefix}Transformation.class);
    private ${classnamePrefix}TransformationConfig config;

    @Override
    public R apply(R record) {
        LOGGER.debug("Applying transformation");
        return null;
    }

    @Override
    public ConfigDef config() {
        return ${classnamePrefix}TransformationConfig.configDef();
    }

    @Override
    public void close() {
        LOGGER.debug("Closing transformation");
        // Close any resource you might have open
    }

    @Override
    public void configure(Map<String, ?> configs) {
        LOGGER.debug("Configuring transformation");
        config = new ${classnamePrefix}TransformationConfig(configs);
    }
}
