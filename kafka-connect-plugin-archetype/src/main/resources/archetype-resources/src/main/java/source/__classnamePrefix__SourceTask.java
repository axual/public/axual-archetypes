#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.source;

import ${package}.common.ConnectorVersion;
import org.apache.kafka.connect.source.SourceRecord;
import org.apache.kafka.connect.source.SourceTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class ${classnamePrefix}SourceTask extends SourceTask {
    private static final Logger LOGGER = LoggerFactory.getLogger(${classnamePrefix}SourceTask.class);
    private ${classnamePrefix}SourceConfig config;

    @Override
    public String version() {
        return ConnectorVersion.getVersion();
    }

    @Override
    public void start(Map<String, String> props) {
        LOGGER.debug("Starting task");
        config = new ${classnamePrefix}SourceConfig(props);
    }

    @Override
    public List<SourceRecord> poll() throws InterruptedException {
        LOGGER.debug("Polling for records");
        return Collections.emptyList();
    }

    @Override
    public void stop() {
        LOGGER.debug("Stopping task");

    }
}
