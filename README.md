# Axual Archetypes

This repository contains archetypes for several project types

## Kafka Connect Plugins
Provides a new Kafka Connect Plugin project, with Kafka Connect test framework attached.

The following command generates a new Maven project

```bash
mvn archetype:generate \
  -DinteractiveMode=false \
  -DarchetypeGroupId=io.axual.archetypes \
  -DarchetypeArtifactId=kafka-connect-plugin-archetype \
  -DarchetypeVersion=0.0.1-alpha-1 \
  -DgroupId=io.axual.connect.example.http \
  -DartifactId=example-http-connect-plugins \
  -Dversion=0.0.1-SNAPSHOT \
  -Dpackage=io.axual.connect.example.http\
  -DclassnamePrefix=ExampleHttp
```

## License

Axual Archetypes is licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt).

